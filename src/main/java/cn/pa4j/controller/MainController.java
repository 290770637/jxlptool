package cn.pa4j.controller;

import cn.hutool.core.io.resource.ResourceUtil;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.net.URL;

public class MainController {

    @FXML
    private Button btn_uuid;

    @FXML
    public void showUUIDTool(){
        try {
            URL url = ResourceUtil.getResource("uuid.fxml");
            Parent anotherRoot = FXMLLoader.load(url);
            Stage anotherStage = new Stage();
            anotherStage.setTitle("UUID生成工具");
            anotherStage.setScene(new Scene(anotherRoot, 400, 600));

            TextField uuid_count = (TextField) anotherRoot.lookup("#uuid_count");
            uuid_count.setText("1");

            anotherStage.show();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
